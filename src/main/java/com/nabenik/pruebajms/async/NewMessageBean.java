/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nabenik.pruebajms.async;

import com.nabenik.pruebajms.controller.PeliculaSessionBean;
import com.nabenik.pruebajms.model.Pelicula;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;


@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "java:app/jms/mpQueue")
    ,
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class NewMessageBean implements MessageListener {
    
    @EJB
    PeliculaSessionBean peliculaService; 
    @PostConstruct
    public void init(){
        System.out.println("Construyendo mi mdb");
    }
    
    @Override
    public void onMessage(Message message) {
        
        try {
            Pelicula pelicula = message.getBody(Pelicula.class);
            
            peliculaService.create(pelicula);
            
        } catch (JMSException ex) {
            Logger.getLogger(NewMessageBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
