/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nabenik.pruebajms.controller;

import com.nabenik.pruebajms.model.Pelicula;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Queue;

@RequestScoped
public class ConsumerBean {

     @Inject
    JMSContext context;
    
    @Resource(lookup = "java:app/jms/mpQueue")
    Queue queue;
    
    
    public String consumirMensaje(){
        return "El mensaje es : " + context.createConsumer(queue).receiveBody(String.class);
        
    }
    
    public Pelicula consumirPelicula(){
         try {
             Thread.sleep(20);
         } catch (InterruptedException ex) {
             Logger.getLogger(ConsumerBean.class.getName()).log(Level.SEVERE, null, ex);
         }
        
        return context.createConsumer(queue).receiveBody(Pelicula.class);
        
    }
    
}
