/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nabenik.pruebajms.controller;

import com.nabenik.pruebajms.model.Pelicula;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Queue;

@RequestScoped
public class ProducerBean {
    
    @Inject
    JMSContext context;
    
    @Resource(lookup = "jms/mpQueue")
    Queue queue;
    
    
    public void producirMensaje(String mensaje){
        context.createProducer().send(queue, mensaje);
        
    }
    
    public void producirPelicula(Pelicula pelicula){
        context.createProducer().send(queue, pelicula);
        
    }
    
}
