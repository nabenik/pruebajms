/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nabenik.pruebajms.controller;

import com.nabenik.pruebajms.model.Pelicula;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class PeliculaSessionBean {

    @PersistenceContext
    EntityManager em;
    
    public void create(Pelicula pelicula){
        try {
            Thread.sleep(15000);
        } catch (InterruptedException ex) {
            Logger.getLogger(PeliculaSessionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        em.persist(pelicula);
        System.out.println("La pelicula ha sido persistida " + pelicula);
    }
    
    
}
