/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nabenik.pruebajms.rest;

import com.nabenik.pruebajms.controller.ConsumerBean;
import com.nabenik.pruebajms.model.Pelicula;
import javax.ws.rs.Path;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/consumir-pelicula")
@RequestScoped
public class PeliculaConsumerEndpoint {

    
   @Inject
   ConsumerBean consumerBean;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Pelicula getPelicula(){
        Pelicula pelicula = consumerBean.consumirPelicula();
        return pelicula;
    }
    
    
}
