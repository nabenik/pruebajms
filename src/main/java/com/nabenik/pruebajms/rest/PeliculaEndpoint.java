/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nabenik.pruebajms.rest;

import com.nabenik.pruebajms.controller.ProducerBean;
import com.nabenik.pruebajms.model.Pelicula;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.core.MediaType;

@Path("/pelicula")
@RequestScoped
public class PeliculaEndpoint {

    
   @Inject
   ProducerBean producerBean;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void postJson(Pelicula content) {
        producerBean.producirPelicula(content);
    }
    
    @GET
    public String ping(){
        return "It works!";
    }
    
    
}
