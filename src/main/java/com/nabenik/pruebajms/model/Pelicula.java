package com.nabenik.pruebajms.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class Pelicula implements Serializable{
    
    @Id
    @Column
    private Integer id;
    
    @Column
    private String mensaje;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return "Pelicula{" + "id=" + id + ", mensaje=" + mensaje + '}';
    }
    
}
